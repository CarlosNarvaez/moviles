package com.example.tabsss

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PagerAdapter (fm : FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position){
            0->{
                //now create three fragments
                FragmentOne()
            }

            1->{
                FragmentTwo()}else->{
                return FragmentThree()
            }
            //this method is set  our tabs positions
        }
    }

    override fun getCount(): Int {
        return 3
        //and this method will return 3 tabs
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position){
            0-> "One"
            1-> "Two"
            else->{
                return "Three"
            }
            //this method will set our tabs title
            //
        }
    }
}