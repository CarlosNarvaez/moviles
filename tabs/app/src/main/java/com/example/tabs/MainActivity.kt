package com.example.tabs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //set our toolBar Title
        toolBar.setTitle("Tab Layout")
        setSupportActionBar(toolBar)

        //these lines are set our adapter
        val fragmAdapter= PagerAdapter(supportFragmentManager)
        viewPage.adapter = fragmAdapter

        //this line will our tabLayout with viewpager
        tableLayout.setupWithViewPager(viewPage)
    }
}
