package com.example.gamecnat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore


class ScoresRVAdapter: RecyclerView.Adapter<ScoresViewHolder>(){

    //RECUPERAR DATOS DE LA BASE

    //referencia al firestore
    val db= FirebaseFirestore.getInstance()
    //Arreglo de los jugadores
    val players1: MutableList<Playerssss> = mutableListOf<Playerssss>()


        //referencia a la coleccion

    init{
        val playersRef= db.collection("players")

        playersRef
            .orderBy("score")
            .addSnapshotListener{snapshot, error ->
                if(error != null){
                    return@addSnapshotListener
                }
                //me sirve para escuchar todos los que tengan la aplicacion, notifica a todos los elementos en tiempo real
                //Log.d("ADAPTER","INFO: ${snapshot!!.metadata}")
                for(doc in snapshot!!.documentChanges){

                    when(doc.type){
                        DocumentChange.Type.ADDED ->{
                            val player = Playerssss(
                                doc.document.getString("name")!!,
                                doc.document.getDouble("score")!!.toInt()
                            )
                            //Log.d("ADAPTER","INFO: ${snapshot!!.metadata}")
                            players1.add(player)
                            notifyItemInserted(players1.size+1 )
                        }
                        else -> return@addSnapshotListener
                    }

                }
            }
    }







                //RECUPERA EL VIEW HOLDER
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScoresViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.scores_view_holder, parent, false)
        return ScoresViewHolder(view)
    }

   //VER A QUIEN CORRESPONDE EL ITEM
    override fun onBindViewHolder(holder: ScoresViewHolder, position: Int) {
       holder.player_name.text= players1[position].name
       holder.player_scores.text = players1[position].score.toString()
    }

    //PARA VER LA CANTIDAD DE ITEMS EXISTENTES
    override fun getItemCount(): Int {
       return players1.size
    }




}