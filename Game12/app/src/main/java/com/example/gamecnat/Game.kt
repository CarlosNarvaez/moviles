package com.example.gamecnat


import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import com.google.firebase.firestore.FirebaseFirestore

import org.w3c.dom.Text


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Game : Fragment() {

    internal lateinit var valTime: TextView
    internal lateinit var valScore: TextView
    internal lateinit var valMessage: TextView
    internal lateinit var valPlay: Button

    private val args: GameArgs by navArgs()

    //Declaraciones de variables

    @State
    var score=0

    @State
    var gameStarted= false

    //Para el tiempo
    @State
    var timeLeft=10
    //Estos nombres de variables son por default
    private val initialCountDown: Long=10000
    private val countDownInterval: Long=100
    private lateinit var countDownTimer: CountDownTimer

    //Variables para usar la BD
    val db= FirebaseFirestore.getInstance()
    var name=""







    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        StateSaver.restoreInstanceState(this,savedInstanceState)

        valTime= view.findViewById(R.id.textView_time)
        valTime.text=getString(R.string.textview_time,timeLeft)


        valScore=view.findViewById(R.id.textView_score)
        valScore.text=getString(R.string.textview_score,score)



        valMessage= view.findViewById(R.id.welcome_message)
        val nombre= args.passName
        valMessage.text=getString(R.string.welcome_message,nombre.toString())



        valPlay=view.findViewById(R.id.button_play)
        valPlay.setOnClickListener { incrementScore() }


        startDownTime()
    }

    private fun incrementScore(){
    if(!gameStarted){
    countDownTimer.start()
    gameStarted=true
    }
        score++
        valScore.text=getString(R.string.textview_score,score)
    }

    private fun startDownTime(){
        score = 0
        valScore.text = getString(R.string.textview_score, score)

        timeLeft = 10
        valTime.text = getString(R.string.textview_time, timeLeft)

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onFinish() {
                endGame()
                //startDownTime()

            }
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                valTime.text = getString(R.string.textview_time, timeLeft)
            }
        }
        gameStarted = false
    }


    private fun endGame(){
        Toast.makeText( //Toast es el mensaje de confirmacion q vemos
            activity,
            getString(R.string.end_game, score),
            Toast.LENGTH_LONG).show()

        val data= HashMap<String, Any>()
        data ["name"] = name
        data ["score"] = score

        db.collection("players")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Log.w("MAIN_GAME","DocumentSnapshot added with  ID: ${documentReference.id}")
            }
            .addOnFailureListener{e->
                Log.w("","Error adding document",e)
            }

       startDownTime()

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game, container, false)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        StateSaver.saveInstanceState(this, outState)
        countDownTimer.cancel()
    }




    }
