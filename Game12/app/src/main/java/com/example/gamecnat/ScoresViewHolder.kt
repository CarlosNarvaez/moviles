package com.example.gamecnat

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ScoresViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val player_name=itemView.findViewById<TextView>(R.id.name_player_score)
    val player_scores= itemView.findViewById<TextView>(R.id.score_player_score)

}