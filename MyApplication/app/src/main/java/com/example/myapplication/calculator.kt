package com.example.myapplication

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.fragment.navArgs
import java.time.temporal.Temporal


class calculator : Fragment() {
    internal lateinit var Roperador:TextView
    internal lateinit var numm1:TextView
    internal lateinit var numm2:TextView
    internal lateinit var Eviar:Button
    internal lateinit var Respuesta:TextView
    internal lateinit var puntaje: TextView
    internal lateinit var tiempo: TextView

    var randomNumber1=0
    var randomNumber2=0
    var score=0
    var resultado=0
    //var gameStarted = false
    var timeleft=10

    // implementacion del tiempo restante
    private var initialCountDown: Long = 10000
    private var countDownInterval: Long =100
    private lateinit var countDownTimer: CountDownTimer

    //para poder pasar los argumentos
    private val args:calculatorArgs by navArgs()



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        //Aqui paso el signo al texEdit de la pantalla de la calculadora
        Roperador=view.findViewById(R.id.signo)
        val opps= args.operador
        Roperador.text=getString(R.string.operador,opps.toString())


        //Numeros Randoms
        numm1=view.findViewById(R.id.num1)
        randomNumber1=(0..10).shuffled().first()
        numm1.text= randomNumber1.toString()


        numm2=view.findViewById(R.id.num2)
        randomNumber1=(0..10).shuffled().first()
        numm2.text= randomNumber1.toString()

        //tiempo restante
        tiempo= view.findViewById(R.id.time)
        tiempo.text= getString(R.string.tiempo, timeleft)



        //Resultado
        resultado=randomNumber1+randomNumber2

        //boton enviar
        Eviar=view.findViewById(R.id.button_contestar)
        Eviar.setOnClickListener{ empezar() }
        //startGame()


        puntaje=view.findViewById(R.id.puntaje)
    }

    fun Suma(){
        if((Respuesta.text.toString().toInt())== resultado){
            score=100
            startGame()
        }
        else{
            score=0
        }
        puntaje.text=score.toString()
    }

    private fun startGame (){
        countDownTimer.start()
        //gameStarted= true
    }


    private fun empezar (){



        countDownTimer= object :CountDownTimer(initialCountDown,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeleft = millisUntilFinished.toInt() / 1000
                tiempo.text =getString(R.string.tiempo, timeleft)
            }

            override fun onFinish() {


            }

        }
    }
















    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calculator, container, false)
    }


    companion object {

    }
}
