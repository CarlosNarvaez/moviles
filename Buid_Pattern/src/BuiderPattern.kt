interface Pizza {
    val salsas : String
    val masa : String
    fun precio()


}

class Hawaiana (override val salsas: String, override val masa: String): Pizza {
    override fun precio() {
        println("Pizza Hawaiana \n" +
                "precio: $ 50  , Salsa: $salsas, Tipo de masa: $masa ")
    }


}

class Queso_Pepperoni (override val salsas: String, override val masa: String): Pizza {
    override fun precio() {
        println("Pizza de Queso_Pepperoni \n" +
                "precio: $ 80  , Salsa: $salsas, Tipo de masa: $masa ")
    }


}

class PizzaBuilder  {
    fun getPizza(pizzaType: String): Pizza {
        return when(pizzaType.trim()) {
            "Pizza Hawaiana" -> Hawaiana("DULCES","SUAVE")
            "Pizza de Queso_Pepperoni" -> Queso_Pepperoni("PICANTES","DURA")

            else -> throw RuntimeException("Unknown shape $pizzaType")
        }
    }

}

fun main(args: Array<String>) {
    val makePizza= PizzaBuilder()

    val Pizza1: Pizza = makePizza.getPizza("Pizza Hawaiana")
    Pizza1.precio()

    val Pizza2: Pizza = makePizza.getPizza("Pizza de Queso_Pepperoni")
    Pizza2.precio()

}