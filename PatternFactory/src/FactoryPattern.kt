interface PrendasVestir {
    val color : String
    val marca : String
    fun precio()
    fun material()

}

class deportivo (override val color: String, override val marca: String): PrendasVestir {
    override fun precio() {
        println("Prenda Deportiva \n" +
                "precio: $ 50  , color: $color, marca: $marca ")
    }
    override fun material(){
        println("Su prenda es fabricada de material tipo : Algodón")
    }

}

class casual (override val color: String, override val marca: String): PrendasVestir {
    override fun precio() {
        println("Prenda Casual \n" +
                "precio: $ 85 , color: $color, marca: $marca ")
    }
    override fun material(){
        println("Su prenda es fabricada de material tipo : Casimir")
    }

}

class verano (override val color: String, override val marca: String): PrendasVestir {
    override fun precio() {
        println("Prenda Para Verano \n" +
                "precio: $ 40 , color: $color, marca: $marca ")
    }
    override fun material(){
        println("Su prenda es fabricada de material tipo : Encaje")
    }

}

class PrendasFactory  {
    fun getPrendas(prendasType: String): PrendasVestir {
        return when(prendasType.trim()) {
            "Prenda Deportiva" -> deportivo("blanco","ADIDAS")
            "Prenda Casual" -> casual("verde","GUCCI")
            "Prenda de Verano" -> verano("rojo","REEBOK")
            else -> throw RuntimeException("Unknown shape $prendasType")
        }
    }

}

fun main(args: Array<String>) {
    val prendasFactory = PrendasFactory()

    val Prenda1: PrendasVestir = prendasFactory.getPrendas("Prenda Deportiva")
    Prenda1.precio()
    Prenda1.material()

    val Prenda2: PrendasVestir = prendasFactory.getPrendas("Prenda Casual")
    Prenda2.precio()
    Prenda2.material()

    val Prenda3: PrendasVestir = prendasFactory.getPrendas("Prenda de Verano")
    Prenda3.precio()
    Prenda3.material()
}